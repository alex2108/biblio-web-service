package org.home.service;

import org.home.model.Journal;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;

public class JournalServiceImplTest extends BaseDaoImplTest {

    @Test
    public void testJournalInsert() {
        Journal journal = new Journal();
        journal.setName("Some new ideas");
        journal.setISSN("111-2222");
        journal.setEdition("O'Reilly");
        journal.setImpactFactor(new BigDecimal("2.35"));
        journalService.save(journal);
        assertTrue(journal.getId() > 0);
        Journal journalDB = journalService.findById(journal.getId());
        assertAll(
                () -> assertTrue(journal.getId() > 0),
                () -> assertEquals("Some new ideas", journalDB.getName()),
                () -> assertEquals("111-2222", journalDB.getISSN()),
                () -> assertEquals("O'Reilly", journalDB.getEdition()),
                () -> assertEquals(new BigDecimal("2.35"), journalDB.getImpactFactor())
        );

        journalService.delete(journal);
        assertNull(journalService.findById(journal.getId()));
    }

    @Test
    public void testFindJournal() {
        List<Journal> journals = journalService.findByEdition("Elsevier");
        assertAll(
                () -> assertEquals(2, journals.size()),
                () -> assertTrue(journals.stream().map(Journal::getName).collect(Collectors.toList())
                        .containsAll(Arrays.asList("Lecture of Computer Science", "European Journal of OR")))
        );
    }

}
