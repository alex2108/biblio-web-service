package org.home.service;

import org.home.config.DataJpaConfig;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;

public class BaseDaoImplTest {

    private static GenericApplicationContext ctx;
    protected AuthorService authorService;
    protected JournalService journalService;
    protected PaperService paperService;

    @BeforeAll
    public static void setUp() {
        ctx = new AnnotationConfigApplicationContext(DataJpaConfig.class);
    }

    @BeforeEach
    public void setBean() {
        authorService = ctx.getBean(AuthorService.class);
        journalService = ctx.getBean(JournalService.class);
        paperService = ctx.getBean(PaperService.class);
        assertAll(
                () -> assertNotNull(authorService),
                () -> assertNotNull(journalService),
                () -> assertNotNull(paperService)
        );
    }

    @AfterAll
    public static void closeCtx() {
        ctx.close();
    }

}
