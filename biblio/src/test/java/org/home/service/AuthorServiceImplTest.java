package org.home.service;

import org.home.model.Author;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertAll;


public class AuthorServiceImplTest extends BaseDaoImplTest {

    @Test
    public void testInsertAuthor() {
        Author author = new Author();
        author.setFirstName("Ivan");
        author.setLastName("Petrov");
        author.setPatronymic("Sergeevich");
        authorService.save(author);
        assertTrue(author.getId() > 0);

        Author authorDB = authorService.findById(author.getId());
        assertAll(
                () -> assertEquals("Ivan", authorDB.getFirstName()),
                () -> assertEquals("Petrov", authorDB.getLastName()),
                () -> assertEquals("Sergeevich", authorDB.getPatronymic())
        );

        authorService.delete(author);
        assertNull(authorService.findById(author.getId()));
    }

    @Test
    public void testFindAuthor() {
        List<Author> authorsOlga = authorService.findByFirstName("Olga");
        List<Author> authorsIvanov = authorService.findByLastName("Ivanov");
        assertAll(
                () -> assertEquals(1, authorsOlga.size()),
                () -> assertEquals(2, authorsIvanov.size()),
                () -> assertEquals("Sergeeva", authorsOlga.get(0).getLastName()),
                () -> assertTrue(authorsIvanov.stream().map(Author::getFirstName).collect(Collectors.toList()).containsAll(Arrays.asList("Petr", "Alexander")))
        );
    }

}
