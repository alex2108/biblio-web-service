package org.home.service;

import org.home.model.*;

import java.util.List;

public interface JournalService {

    Journal findById(Integer journalId);

    Journal findByISSN(String ISSN);

    List<Journal> findByName(String name);

    List<Journal> findByEdition(String edition);

    Journal findByPaper(Paper paper);

    List<Journal> findAll();

    void save(Journal journal);

    void delete(Journal journal);
}
