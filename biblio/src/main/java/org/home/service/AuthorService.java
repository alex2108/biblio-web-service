package org.home.service;

import org.home.model.Author;

import java.util.List;

public interface AuthorService {

    List<Author> findAll();

    Author findById(Integer id);

    List<Author> findByFirstName(String firstName);

    List<Author> findByLastName(String lastName);

    void save(Author author);

    void delete(Author author);
}
