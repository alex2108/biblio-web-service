package org.home.service;

import org.home.model.*;
import java.util.List;

public interface PaperService {
    Paper findById(Integer id);

    List<Paper> findAll();

    List<Paper> findByTitle(String title);

    List<Paper> findByJournal(Journal journal);

    List<Paper> findByAuthor(Author author);

    void save(Paper paper);

    void delete(Paper paper);
}
