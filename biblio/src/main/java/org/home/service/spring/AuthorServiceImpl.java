package org.home.service.spring;

import org.home.model.Author;
import org.home.repository.AuthorRepository;
import org.home.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jms.core.JmsTemplate;

import java.util.ArrayList;
import java.util.List;

@Service("springJpaDataAuthorService")
@Transactional
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Transactional(readOnly = true)
    @Override
    public List<Author> findAll() {
        return new ArrayList<Author>((List<Author>) authorRepository.findAll());
    }

    @Transactional(readOnly = true)
    @Override
    public Author findById(Integer id) {
        return authorRepository.findById(id).orElse(null);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Author> findByFirstName(String firstName) {
        return authorRepository.findByFirstName(firstName);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Author> findByLastName(String lastName) {
        return authorRepository.findByLastName(lastName);
    }

    @Transactional
    @Override
    public void save(Author author) {
        authorRepository.save(author);
        //if (author != null) {
        //    throw new RuntimeException("Simulation of something  going  wrong.");
        //}
        jmsTemplate.convertAndSend("biblio", "Saved: " + author);
    }

    @Transactional
    @Override
    public void delete(Author author) {
        authorRepository.deleteById(author.getId());
    }
}
