package org.home.service.spring;

import org.home.model.Journal;
import org.home.model.Paper;
import org.home.repository.JournalRepository;
import org.home.service.JournalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("springJpaDataJournalService")
@Transactional
public class JournalServiceImpl implements JournalService {

    @Autowired
    private JournalRepository journalRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Transactional(readOnly = true)
    @Override
    public Journal findById(Integer journalId) {
        return journalRepository.findById(journalId).orElse(null);
    }

    @Transactional(readOnly = true)
    @Override
    public Journal findByISSN(String ISSN) {
        return journalRepository.findByISSN(ISSN);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Journal> findByName(String name) {
        return journalRepository.findByName(name);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Journal> findByEdition(String edition) {
        return journalRepository.findByEdition(edition);
    }

    @Transactional(readOnly = true)
    @Override
    public Journal findByPaper(Paper paper) {
        return journalRepository.findByPaper(paper.getId());
    }

    @Transactional(readOnly = true)
    @Override
    public List<Journal> findAll() {
        return (List<Journal>) journalRepository.findAll();
    }

    @Transactional
    @Override
    public void save(Journal journal) {
        journalRepository.save(journal);
        jmsTemplate.convertAndSend("biblio", "Saved: " + journal);
    }

    @Transactional
    @Override
    public void delete(Journal journal) {
        journalRepository.deleteById(journal.getId());
    }
}
