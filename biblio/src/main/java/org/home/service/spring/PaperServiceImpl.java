package org.home.service.spring;

import org.hibernate.Hibernate;
import org.home.model.Author;
import org.home.model.Journal;
import org.home.model.Paper;
import org.home.repository.AuthorRepository;
import org.home.repository.PaperRepository;
import org.home.service.PaperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;

@Service("springJpaDataPaperService")
@Transactional
public class PaperServiceImpl implements PaperService {

    @Autowired
    private PaperRepository paperRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Transactional(readOnly = true)
    @Override
    public List<Paper> findAll() {
        return new ArrayList<Paper>((List<Paper>) paperRepository.findAll());
    }

    @Transactional(readOnly = true)
    @Override
    public Paper findById(Integer id) {
        Paper paper = paperRepository.findById(id).orElse(null);
        if (paper != null) {
            Hibernate.initialize(paper.getAuthors());
        }
        return paper;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Paper> findByTitle(String title) {
        return paperRepository.findByTitle(title);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Paper> findByJournal(Journal journal) {
        return paperRepository.findByJournal(journal);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Paper> findByAuthor(Author author) {
        List<Paper> papers = paperRepository.findByAuthors_Id(author.getId());
        for (Paper paper : papers) {
            Hibernate.initialize(paper.getAuthors());
        }
        return papers;
    }

    @Transactional
    @Override
    public void save(Paper paper) {
        authorRepository.saveAll(paper.getAuthors());
        paperRepository.save(paper);
        jmsTemplate.convertAndSend("biblio", "Saved: " + paper);
    }

    @Transactional
    @Override
    public void delete(Paper paper) {
        paperRepository.deleteById(paper.getId());
    }
}
