package org.home.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

@Embeddable
public class PageRange {

    @Column(name = "PAGE_START", nullable = false)
    private Integer pageStart;

    @Column(name = "PAGE_END")
    private Integer pageEnd;

    public PageRange() {
    }

    public PageRange(Integer pageStart, Integer pageEnd) {
        this.pageStart = pageStart;
        this.pageEnd = pageEnd;
    }

    public Integer getPageStart() {
        return pageStart;
    }

    public Integer getPageEnd() {
        return pageEnd;
    }

    public void setPageStart(Integer pageStart) {
        this.pageStart = pageStart;
    }

    public void setPageEnd(Integer pageEnd) {
        this.pageEnd = pageEnd;
    }

    @Override
    public String toString() {
        return pageEnd != null ? pageStart + "-" + pageEnd : String.valueOf(pageStart);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PageRange pageRange = (PageRange) o;
        return Objects.equals(pageStart, pageRange.pageStart) &&
                Objects.equals(pageEnd, pageRange.pageEnd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pageStart, pageEnd);
    }
}
