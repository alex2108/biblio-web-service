package org.home.repository;

import org.home.model.Journal;
import org.home.model.Paper;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaperRepository extends CrudRepository<Paper, Integer> {
    List<Paper> findByTitle(String title);
    List<Paper> findByJournal(Journal journal);

    //@Query("select from Paper p " +
    //        "join Author a " +
    //        "where a.id=:authorId")
    List<Paper> findByAuthors_Id(Integer authorId);

}
