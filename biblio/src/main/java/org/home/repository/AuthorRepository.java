package org.home.repository;

import org.springframework.data.repository.CrudRepository;
import org.home.model.Author;

import java.util.List;

public interface AuthorRepository extends CrudRepository<Author, Integer> {
    List<Author> findByFirstName(String firstName);
    List<Author> findByLastName(String lastName);
}
