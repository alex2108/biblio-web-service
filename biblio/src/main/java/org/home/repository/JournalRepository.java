package org.home.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.home.model.Journal;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface JournalRepository extends CrudRepository<Journal, Integer> {

    Journal findByISSN(String issn);

    List<Journal> findByName(String name);

    List<Journal> findByEdition(String edition);

    @Query("select j from Journal j " +
            "join j.papers p " +
            "where p.id=:paperId")
    Journal findByPaper(@Param("paperId") Integer paperId);
}
