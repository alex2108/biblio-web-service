package org.home.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Messages {

    private  static Logger logger = LoggerFactory.getLogger(Messages.class);

    @JmsListener(destination = "biblio")
    public void onMessage(String content) {
        logger.info("Received content: " + content);
        System.out.println("Received content: " + content);
    }

}
