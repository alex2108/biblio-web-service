package org.home;

import org.home.config.DataJpaConfig;
import org.home.model.Author;
import org.home.model.Journal;
import org.home.model.PageRange;
import org.home.model.Paper;
import org.home.service.AuthorService;
import org.home.service.JournalService;
import org.home.service.PaperService;
import org.home.service.spring.AuthorServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SpringBootApplication
public class ApplicationBiblio {

    private  static Logger logger = LoggerFactory.getLogger(ApplicationBiblio.class);

    public static void main(String[] args) {
        //ApplicationContext ctx = SpringApplication.run(ApplicationBiblio.class, args);
        //ApplicationContext ctx = new ClassPathXmlApplicationContext("app-context-annotation.xml");
        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(DataJpaConfig.class);
        if (ctx == null) {
            logger.info("Spring app is not configured");
            return;
        }
        logger.info("Spring app is configured");
        AuthorService authorService = ctx.getBean(AuthorService.class);
        PaperService paperService = ctx.getBean(PaperService.class);
        JournalService journalService = ctx.getBean(JournalService.class);

        addAuthor(authorService);
        addNewPaper(authorService, paperService, journalService);
    }

    private static void addNewPaper(AuthorService authorService, PaperService paperService, JournalService journalService) {
        Set<Author> authors = new HashSet<>(authorService.findByLastName("Smirnov"));
        Journal journal = journalService.findByISSN("521-12345");
        Paper paper = new Paper("Some new patterns", journal, 2019, 10, 1, new PageRange(15, 20));
        paper.setAuthors(authors);
        paperService.save(paper);
    }

    private static void addAuthor(AuthorService authorService) {
        Author author = new Author("Sergey", "Smirnov");
        authorService.save(author);
    }
}
