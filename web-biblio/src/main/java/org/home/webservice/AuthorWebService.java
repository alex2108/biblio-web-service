package org.home.webservice;

import org.home.model.AuthorJaxb;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface AuthorWebService {
    AuthorJaxb getAuthorInfo(Integer id);
    List<AuthorJaxb> getAllAuthors();
}
