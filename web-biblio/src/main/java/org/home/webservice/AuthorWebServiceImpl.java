package org.home.webservice;

import org.home.model.Author;
import org.home.model.Paper;
import org.home.service.AuthorService;
import org.home.model.AuthorJaxb;
import org.home.model.PaperJaxb;
import org.home.service.AuthorServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;


import javax.inject.Inject;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@WebService(endpointInterface = "org.home.webservice.AuthorWebService", serviceName = "authorService")
public class AuthorWebServiceImpl extends SpringBeanAutowiringSupport implements AuthorWebService {

    private  static Logger logger = LoggerFactory.getLogger(AuthorWebServiceImpl.class);

    //@Autowired
    private AuthorService authorService = new AuthorServiceImpl();

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8080/authorService", new AuthorWebServiceImpl());
    }

    @Override
    @WebResult(name = "author")
    public AuthorJaxb getAuthorInfo(@WebParam(name = "id") Integer id) {
        Author author = authorService.findById(id);
        return convert(author);
    }

    @Override
    @WebResult(name = "authors")
    public List<AuthorJaxb> getAllAuthors() {
        List<Author> authors = authorService.findAll();
        List<AuthorJaxb> authorsJaxb = new ArrayList<>();
        for (Author author : authors) {
            authorsJaxb.add(convert(author));
        }
        return authorsJaxb;
    }

    private static AuthorJaxb convert(Author author) {
        AuthorJaxb authorJaxb = new AuthorJaxb();
        authorJaxb.setFirstName(author.getFirstName());
        authorJaxb.setLastName(author.getLastName());
        authorJaxb.setPatronymic(author.getPatronymic());
        authorJaxb.setPapersJaxb(convert(author.getPapers()));
        return authorJaxb;
    }

    private static List<PaperJaxb> convert(Collection<Paper> papers) {
        return papers.stream()
                .map(paper -> {
                    PaperJaxb jaxb = new PaperJaxb();
                    jaxb.setTitle(paper.getTitle());
                    jaxb.setJournal(paper.getJournal().getName());
                    jaxb.setYear(paper.getYear().toString());
                    jaxb.setVolume(paper.getIssue());
                    jaxb.setIssue(paper.getIssue());
                    jaxb.setPageStart(paper.getPages().getPageStart());
                    jaxb.setPageEnd(paper.getPages().getPageEnd());
                    return jaxb;
                }).collect(Collectors.toList());
    }
}
