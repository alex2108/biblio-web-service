package org.home.service;

import org.home.model.Author;

import java.util.Arrays;
import java.util.List;

public class AuthorServiceImpl implements AuthorService {
    @Override
    public List<Author> findAll() {
        return Arrays.asList(new Author("Ivan", "Ivanov"), new Author("Petr", "Petrov", "Ivanovich"));
    }

    @Override
    public Author findById(Integer id) {
        return new Author("Ivan", "Ivanov");
    }
}
