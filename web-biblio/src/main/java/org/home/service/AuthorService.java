package org.home.service;

import org.home.model.Author;

import javax.ejb.Remote;
import java.util.List;

public interface AuthorService {

    List<Author> findAll();

    Author findById(Integer id);
}
