package org.home.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Journal {
    private Integer id;
    private String name;
    private String ISSN;
    private String edition;
    private BigDecimal impactFactor;
    private Set<Paper> papers = new HashSet<>();

    public Journal() {
    }

    public Journal(String name, String ISSN, String edition, BigDecimal impactFactor) {
        this.name = name;
        this.ISSN = ISSN;
        this.edition = edition;
        this.impactFactor = impactFactor;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getISSN() {
        return ISSN;
    }

    public String getEdition() {
        return edition;
    }

    public BigDecimal getImpactFactor() {
        return impactFactor;
    }

    public Set<Paper> getPapers() {
        return papers;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setISSN(String ISSN) {
        this.ISSN = ISSN;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public void setImpactFactor(BigDecimal impactFactor) {
        this.impactFactor = impactFactor;
    }

    public void setPapers(Set<Paper> papers) {
        this.papers = papers;
    }

    @Override
    public String toString() {
        return "Journal{" +
                "name='" + name + '\'' +
                ", edition='" + edition + '\'' +
                '}';
    }
}
