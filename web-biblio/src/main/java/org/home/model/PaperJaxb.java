package org.home.model;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "paper")
@XmlAccessorType(XmlAccessType.FIELD)
public class PaperJaxb {

    @XmlElement(required = true)
    private String title;

    @XmlElement(required = true)
    private String journal;

    @XmlElement(required = true)
    private String year;

    private Integer volume;

    private Integer issue;

    @XmlElement(required = true)
    private Integer pageStart;

    private Integer pageEnd;

    public PaperJaxb() {
    }

    public String getTitle() {
        return title;
    }

    public String getJournal() {
        return journal;
    }

    public String getYear() {
        return year;
    }

    public Integer getVolume() {
        return volume;
    }

    public Integer getIssue() {
        return issue;
    }

    public Integer getPageStart() {
        return pageStart;
    }

    public Integer getPageEnd() {
        return pageEnd;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setJournal(String journal) {
        this.journal = journal;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public void setIssue(Integer issue) {
        this.issue = issue;
    }

    public void setPageStart(Integer pageStart) {
        this.pageStart = pageStart;
    }

    public void setPageEnd(Integer pageEnd) {
        this.pageEnd = pageEnd;
    }
}
