package org.home.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "author")
@XmlAccessorType(XmlAccessType.FIELD)
public class AuthorJaxb {
    @XmlElement(required = true)
    private String firstName;
    @XmlElement(required = true)
    private String lastName;

    private String patronymic;
    @XmlElementWrapper(name = "papers")
    @XmlElement(name = "paper")
    private List<PaperJaxb> papersJaxb = new ArrayList<>();

    public AuthorJaxb() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public List<PaperJaxb> getPapersJaxb() {
        return papersJaxb;
    }

    public void setPapersJaxb(List<PaperJaxb> papersJaxb) {
        this.papersJaxb = papersJaxb;
    }
}
