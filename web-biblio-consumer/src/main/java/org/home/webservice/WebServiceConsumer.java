package org.home.webservice;

import javax.xml.ws.WebServiceRef;

public class WebServiceConsumer {
    @WebServiceRef
    private static AuthorWebServiceImplService authorWebService;

    public static void main(String[] args) {
        System.out.println("authorWebService = " + authorWebService);
        AuthorWebService authorService = authorWebService.getAuthorWebServiceImplPort();
        System.out.println("authorService = " + authorService);
        AuthorJaxb author = authorService.getAuthorInfo(1);
        System.out.println(author);
    }

}
